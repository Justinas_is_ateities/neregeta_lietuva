<?php include './partials/header.php';?>
	
	<div class="page index_page">
		<section class="index_hero">
			<div class="hero_video_background desktop_only"
				data-vide-bg="mp4: ./media/video/marius.mp4, poster: ./media/images/index_hero.jpg"
				data-vide-options="posterType: jpg, loop: true, muted: true, position: 50% 50%">
			</div>
			<div class="mobile_background mobile_only" style="background-image: url('./media/images/index_hero.jpg');"></div>
			<div class="wrapper">
				<h1>Naujoji knyga pristatys mūsų šalį trimis skirtingais kampais</h1>
				<div class="boxes">
					<a href="#" class="box">
						<div class="name">KRAŠTOVAIZDIS</div>
						<div class="content">
							Lorem ipsum dolorsit aconsect adipiscing elitsed do eiusmod tempor incididunt ut labore et dolore magna Lorem ipsum dolorsit aconsect adipiscing elitsed do eiusmod tempor incididunt ut labore et dolore magna
						</div>
					</a>
					<a href="#" class="box">
						<div class="name">PAVELDAS</div>
						<div class="content">
							Lorem ipsum dolorsit aconsect adipiscing elitsed do eiusmod tempor incididunt ut labore et dolore magna Lorem ipsum dolorsit aconsect adipiscing elitsed do eiusmod tempor incididunt ut labore et dolore magna
						</div>
					</a>
					<a href="#" class="box">
						<div class="name">ATEITIS</div>
						<div class="content">
							Lorem ipsum dolorsit aconsect adipiscing elitsed do eiusmod tempor incididunt ut labore et dolore magna Lorem ipsum dolorsit aconsect adipiscing elitsed do eiusmod tempor incididunt ut labore et dolore magna
						</div>
					</a>
				</div>
			</div>
		</section>
		<section class="books">
			<div class="wrapper">
				<div class="book_block">
					<div class="img_holder">
						<img src="./media/images/book.jpg" alt="">
						<div class="pattern" data-pop-book>
							<div class="center">
								<div class="icon"></div>
								<div class="text">knygos pristatymas</div>
							</div>
						</div>
					</div>
					<div class="title">Nauja Neregėta Lietuva</div>
					<div class="about">
						Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore.
					</div>
					<div class="langs">
						Kalba: Lietuvių
					</div>
					<div class="price">79€</div>
					<a href="#" class="button blue"><span>Pirkti knygą</span></a>
				</div>
				<div class="book_block">
					<div class="img_holder">
						<img src="./media/images/book.jpg" alt="">
						<div class="pattern" data-pop-book>
							<div class="center">
								<div class="icon"></div>
								<div class="text">knygos pristatymas</div>
							</div>
						</div>
					</div>
					<div class="title">Nauja Neregėta Lietuva</div>
					<div class="about">
						Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore.
					</div>
					<div class="langs">
						Kalba: Lietuvių
					</div>
					<div class="price">79€</div>
					<a href="#" class="button blue"><span>Pirkti knygą</span></a>
				</div>
				<div class="book_block">
					<div class="img_holder">
						<img src="./media/images/book.jpg" alt="">
						<div class="pattern" data-pop-book>
							<div class="center">
								<div class="icon"></div>
								<div class="text">knygos pristatymas</div>
							</div>
						</div>
					</div>
					<div class="title">Nauja Neregėta Lietuva</div>
					<div class="about">
						Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore.
					</div>
					<div class="langs">
						Kalba: Lietuvių
					</div>
					<div class="price">79€</div>
					<a href="#" class="button blue"><span>Pirkti knygą</span></a>
				</div>
				<div class="book_block">
					<div class="img_holder">
						<img src="./media/images/book.jpg" alt="">
						<div class="pattern" data-pop-book>
							<div class="center">
								<div class="icon"></div>
								<div class="text">knygos pristatymas</div>
							</div>
						</div>
					</div>
					<div class="title">Nauja Neregėta Lietuva</div>
					<div class="about">
						Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore.
					</div>
					<div class="langs">
						Kalba: Lietuvių
					</div>
					<div class="price">79€</div>
					<a href="#" class="button blue"><span>Pirkti knygą</span></a>
				</div>
				<div class="book_block">
					<div class="img_holder">
						<img src="./media/images/book.jpg" alt="">
						<div class="pattern" data-pop-book>
							<div class="center">
								<div class="icon"></div>
								<div class="text">knygos pristatymas</div>
							</div>
						</div>
					</div>
					<div class="title">Nauja Neregėta Lietuva</div>
					<div class="about">
						Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore.
					</div>
					<div class="langs">
						Kalba: Lietuvių
					</div>
					<div class="price">79€</div>
					<a href="#" class="button blue"><span>Pirkti knygą</span></a>
				</div>
				<div class="book_block">
					<div class="img_holder">
						<img src="./media/images/book.jpg" alt="">
						<div class="pattern" data-pop-book>
							<div class="center">
								<div class="icon"></div>
								<div class="text">knygos pristatymas</div>
							</div>
						</div>
					</div>
					<div class="title">Nauja Neregėta Lietuva</div>
					<div class="about">
						Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore.
					</div>
					<div class="langs">
						Kalba: Lietuvių
					</div>
					<div class="price">79€</div>
					<a href="#" class="button blue"><span>Pirkti knygą</span></a>
				</div>
			</div>
		</section>
		<section class="add">
			<img src="./media/images/add.jpg" alt="">
		</section>
		<section class="witnessing">
			<div class="wrapper">
				<h2>Apie knygą kalba</h2>
				<div class="blocks_holder">
					<div class="witness">
						<div class="left">

							<!-- Jeigu nera video, tai imesk viska, kas yra video_holder viduje ir tiesiog uzdek paveiksliuka per style: -->

							<div class="video_holder" style="background-image: url('./media/images/video.jpg');"></div>
						</div>
						<div class="right">
							<div class="text_holder">
								Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore. Magna aliqua ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.
								<div class="author">Andrius Mamontovas</div>
							</div>
						</div>
					</div>
					<div class="witness">
						<div class="left">
							<div class="text_holder">
								Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore. Magna aliqua ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.
								<div class="author">Andrius Mamontovas</div>
							</div>
						</div>
						<div class="right">

							<!-- Jeigu yra video, tai darom taip -->

							<div class="video_holder">
								<div class="poster" style="background-image: url('./media/images/video.jpg');"></div>
								<iframe src="https://www.youtube-nocookie.com/embed/SlbVgjFvE3I?rel=0" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
							</div>
						</div>
					</div>
				</div>
				<div class="buttons_holder">
					<a href="#" class="button raw">ŽIŪRĖTI DAUGIAU</a>
				</div>
			</div>
		</section>
		<section class="video_background">
			<div class="wrapper">
				<div class="left">
					<div class="text_holder">
						<h2>Marius Jovaiša</h2>
						<div class="text">
							Fotografas ir leidėjas, verslininkas, aktyvaus ir sveiko gyvenimo būdo skleidėjas. Nuo 2006 metų jo įsteigta leidykla „Unseen Pictures” leidžia unikalias oro fotografijų knygas apie Lietuvą ir kitas pasaulio šalis (Meksiką, Belizą,Kubą).<br/><br/>
							2008-aisiais metais išleistas pirmasis albumo „Neregėta Lietuva” leidimas išverstas į 11 kalbų, Mariaus Jovaišos nuotraukų parodos surengtos beveik 40 valstybių. Už nuopelnus Lietuvos įvaizdžio ir turizmo vystymui apdovanotas Lietuvos Respublikos Vyriausybės ir Respublikos Prezidento.
						</div>
						<a href="#" class="button raw">ŽIŪRĖTI DARBUS</a>
					</div>
				</div>
				<video id="author_video" src="./media/video/marius.mp4" muted playsinline></video>
			</div>
		</section>
		<section class="news">
			<div class="wrapper">
				<h2>Naujienos</h2>
				<div class="news_grid">
					<div class="grid_sizer"></div>
					<a href="#" class="new big">
						<span class="img" style="background-image: url('./media/images/new.jpg')"></span>
						<span class="info">
							<span class="icon"></span>
							<span class="text">
								Magna aliquaut enim adminim veniam quis nostrud ullamco
								<span class="date">2018 spalio 15 d.</span>
							</span>
						</span>
					</a>
					<a href="#" class="new">
						<span class="img" style="background-image: url('./media/images/new.jpg')"></span>
						<span class="info">
							<span class="icon"></span>
							<span class="text">
								Magna aliquaut enim adminim veniam quis nostrud ullamco
								<span class="date">2018 spalio 15 d.</span>
							</span>
						</span>
					</a>
					<a href="#" class="new">
						<span class="img" style="background-image: url('./media/images/new.jpg')"></span>
						<span class="info">
							<span class="icon"></span>
							<span class="text">
								Magna aliquaut enim adminim veniam quis nostrud ullamco
								<span class="date">2018 spalio 15 d.</span>
							</span>
						</span>
					</a>
					<a href="#" class="new">
						<span class="img" style="background-image: url('./media/images/new.jpg')"></span>
						<span class="info">
							<span class="icon"></span>
							<span class="text">
								Magna aliquaut enim adminim veniam quis nostrud ullamco
								<span class="date">2018 spalio 15 d.</span>
							</span>
						</span>
					</a>
					<a href="#" class="new big">
						<span class="img" style="background-image: url('./media/images/new.jpg')"></span>
						<span class="info">
							<span class="icon"></span>
							<span class="text">
								Magna aliquaut enim adminim veniam quis nostrud ullamco
								<span class="date">2018 spalio 15 d.</span>
							</span>
						</span>
					</a>
					<a href="#" class="new">
						<span class="img" style="background-image: url('./media/images/new.jpg')"></span>
						<span class="info">
							<span class="icon"></span>
							<span class="text">
								Magna aliquaut enim adminim veniam quis nostrud ullamco
								<span class="date">2018 spalio 15 d.</span>
							</span>
						</span>
					</a>
					<a href="#" class="new">
						<span class="img" style="background-image: url('./media/images/new.jpg')"></span>
						<span class="info">
							<span class="icon"></span>
							<span class="text">
								Magna aliquaut enim adminim veniam quis nostrud ullamco
								<span class="date">2018 spalio 15 d.</span>
							</span>
						</span>
					</a>
					<a href="#" class="new medium">
						<span class="img" style="background-image: url('./media/images/new.jpg')"></span>
						<span class="info">
							<span class="icon"></span>
							<span class="text">
								Magna aliquaut enim adminim veniam quis nostrud ullamco
								<span class="date">2018 spalio 15 d.</span>
							</span>
						</span>
					</a>
					<a href="#" class="new">
						<span class="img" style="background-image: url('./media/images/new.jpg')"></span>
						<span class="info">
							<span class="icon"></span>
							<span class="text">
								Magna aliquaut enim adminim veniam quis nostrud ullamco
								<span class="date">2018 spalio 15 d.</span>
							</span>
						</span>
					</a>
				</div>
				<div class="buttons_holder">
					<a href="#" class="button raw">Daugiau naujienų</a>
				</div>
			</div>
		</section>
		<section class="partners">
			<div class="wrapper">
				<div class="name">Kiti projekto partneriai:</div>
				<div class="parners_holder">
					<div class="partner">
						<img src="./media/images/delfi.png" alt="">
					</div>
					<div class="partner">
						<img src="./media/images/lrt.png" alt="">
					</div>
					<div class="partner">
						<img src="./media/images/delfi.png" alt="">
					</div>
					<div class="partner">
						<img src="./media/images/lrt.png" alt="">
					</div>
				</div>
			</div>
		</section>
	</div>

<?php include './partials/footer.php';?>