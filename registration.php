<?php include './partials/header.php';?>

	<div class="page solo_form_page registration_page">
		<form class="wrapper">
			<h2>Registracija</h2>
			<div class="simple_input">
				<input type="text" name="name">
				<label>Vardas*</label>
				<div class="error_msg">Lorem ipsum dolor</div>
			</div>
			<div class="simple_input">
				<input type="text" name="surname">
				<label>Pavardė*</label>
				<div class="error_msg">Lorem ipsum dolor</div>
			</div>
			<div class="simple_input">
				<input type="email" name="email">
				<label>El. paštas*</label>
				<div class="error_msg">Lorem ipsum dolor</div>
			</div>
			<div class="simple_input">
				<input type="number" name="number">
				<label>Telefono numeris</label>
				<div class="error_msg">Lorem ipsum dolor</div>
			</div>
			<div class="simple_input">
				<input type="password" name="password">
				<label>Slaptažodis*</label>
				<div class="error_msg">Lorem ipsum dolor</div>
			</div>
			<div class="simple_input">
				<input type="password" name="re_password">
				<label>Patvirtinti slaptažodį*</label>
				<div class="error_msg">Lorem ipsum dolor</div>
			</div>
			<div class="text">* Privaloma užpildyti</div>
			<div class="text">
				Jūsų asmeniniai duomenys bus naudojami užsakymo šioje svetainėje įvykdymui, taip pat kitiems tikslams, aprašytiems mūsų <a href="#">taisyklėse ir sąlygose</a>.
			</div>
			<label class="checkbox">
				<input type="checkbox" name="remember">
				<span class="name">Su elektroninės parduotuvės <a href="#">taisyklėmis ir sąlygomis</a> susipažinau ir sutinku.</span>
			</label>
			<button type="submit" class="button blue"><span>Registruotis</span></button>
			<div class="or">arba</div>
			<button type="submit" class="button blue fb"><span>Registruotis su facebook</span></button>
			<button type="submit" class="button blue google"><span>Registruotis su google+</span></button>
		</form>
	</div>

<?php include './partials/footer.php';?>