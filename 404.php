<?php include './partials/header.php';?>

	<div class="page wrong_page">
		<div class="center">
			<img src="./media/images/404.svg" alt="">
			<h2>Puslapis nerastas</h2>
			<div class="text">Lorem ipsum dolor sit amet, consectetur adipiscing elitsed do eiusmod tempor incididunt labore</div>
			<a href="#" class="button raw">GRĮŽTI Į PAGRINDINĮ PUSLAPĮ</a>
		</div>
	</div>

<?php include './partials/footer.php';?>