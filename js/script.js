(function() {
	var popup_debounce = false;
	var dif;

	$(document).ready(function() {
		adjustScrollbar();
		if ($('section.witnessing').is(':visible') || $('section.video').is(':visible')) { video(); }
		if (!$('.index_page').is(':visible') && !$('.black_page').is(':visible')) { 
			$('header').addClass('black'); 
		}
		if ($('section.cart').is(':visible')) { 
			cart();
		}

		if ($('.checkout_page').is(':visible')) {
			$('header').hide();
			$('input[name="company"]').on('change', function() {
				$('.company_toggler').slideToggle(300);
			});
			$('input[name="register"]').on('change', function() {
				$('.register_toggler').slideToggle(300);
			});
		}
		if ($('.success_page').is(':visible') || $('.wrong_page').is(':visible')) {
			$('header').hide();
		}
		$('.pop .close').on('click', function() {
			closePop();
		});
		$('#overlay').on('click', function(e) {
			var target = $(e.target);
			if (!target.hasClass('pop') && !target.closest('.pop').hasClass('pop')) {
				closePop();
			}
		});
		inputs();
		$('[data-pop-book]').on('click', function() {
			openPop('book');
		});
		$('[data-pop-buy]').on('click', function() {
			openPop('buy');
		});

		// openPop('add');
		
		$('video').on("loadstart", function () {
			$(this).get(0).play();
		});

		if ($('section.pay').is(':visible')) {
			var pay = $('section.pay');
			var right;
			var pay_start = pay.offset().top + $(this).scrollTop() - 82;
			function checkRight() {
				if (window.innerWidth > 1180) {
					right = (window.innerWidth - 1180 + dif) / 2 + 40;
				} else {
					right = 40 + dif;
				}
				pay.css({ 'right': right+'px' });
			}
			checkRight();
			$(window).on('resize', function() {
				checkRight();
			});
			$('main').on('scroll', function() {
				var scrolled = $(this).scrollTop();
				if (scrolled >= pay_start && !pay.hasClass('fixed')) {
					pay.addClass('fixed');
				}
				if (scrolled < pay_start && pay.hasClass('fixed')) {
					pay.removeClass('fixed');
				}
			});
		}

	});

	$(window).on('load', function() {
		if ($('section.news').is(':visible')) { 
			var grid = $('.news_grid');
			var grid_inst;
			if (window.innerWidth > 1000 && !grid.hasClass('grid_active')) {
				grid.addClass('grid_active');
				grid_inst = grid.isotope({
					itemSelector: '.new',
					layoutMode: 'masonry',
					masonry: {
						columnWidth: '.grid_sizer',
						gutter: 70
					}
				});
			}
			$(window).on('resize', function() {
				if (window.innerWidth > 1000 && !grid.hasClass('grid_active')) {
					grid.addClass('grid_active');
					grid_inst = grid.isotope({
						itemSelector: '.new',
						layoutMode: 'masonry',
						masonry: {
							columnWidth: '.grid_sizer',
							gutter: 70
						}
					});
				} else if (window.innerWidth <= 1000 && grid.hasClass('grid_active')) {
					grid.removeClass('grid_active');
					grid_inst.isotope('destroy');
					grid_inst = null;
				}
			});
		}
		if ($('section.main_slider').is(':visible')) { 
			var mySwiper = new Swiper ('section.main_slider .swiper-container', {
				grabCursor: true,
				freeMode: true,
      			spaceBetween: 60,
      			slidesPerView: 'auto'
			})
		}
		if ($('section.book_blocks').is(':visible')) { 
			var mySwiper = new Swiper ('section.book_blocks .swiper-container', {
				loop: true,
				grabCursor: true,
				effect: 'coverflow',
				autoplay: {
					delay: 4000,
				},
				speed: 500,
				navigation: {
					nextEl: 'section.book_blocks .swiper-button-next',
					prevEl: 'section.book_blocks .swiper-button-prev',
				}
			})
		}
		if ($('section.full_gallery').is(':visible')) { 
			var mySwiper = new Swiper ('section.full_gallery.swiper-container', {
				loop: true,
				grabCursor: true,
				effect: 'coverflow',
				autoplay: {
					delay: 4000,
				},
				speed: 500,
				navigation: {
					nextEl: 'section.full_gallery .swiper-button-next',
					prevEl: 'section.full_gallery .swiper-button-prev',
				}
			})
		}
	});

	function adjustScrollbar() {
		dif = $('body').outerWidth() - $('#scroller').outerWidth();
		$('header').css({ 'width': 'calc(100% - '+dif+'px)' });
		$('.cookies_box').css({ 'width': 'calc(100% - '+dif+'px)' });
	}

	function productAdded() {
		var cart = $('header .cart');
		cart.addClass('added');
		setTimeout(function() {
			cart.removeClass('added');
		},310);
	}

	(function() {
		var menu_debounce = false;
		var togglers = $('header .lang_selector, header .person');

		$('.burger').on('click', function() {
			if (menu_debounce) {
				return false;
			}
			menu_debounce = true;
			setTimeout(function() {
				menu_debounce = false;
			}, 300);
			$('body').toggleClass('open');
			togglers.removeClass('hover');
		});

		togglers.on('click', function() {
			togglers.not($(this)).removeClass('hover');
			$(this).toggleClass('hover');
		});
	})();

	$('section.menu .overlay').on('click', function() {
		$('body').removeClass('open');
	});

	(function() {
		var body = $('body');
		$('main').on('scroll', function() {
			var scrolled = $(this).scrollTop();
			if (scrolled > 30 && !body.hasClass('scrolled')) {
				body.addClass('scrolled');
			} else if (scrolled <= 30 && body.hasClass('scrolled')) {
				body.removeClass('scrolled');
			}
		});
	})();

	function video() {

		$('.poster').on('click', function() {
			$(this).addClass('gone');
		});
	}
	(function() {
		var toggler_debounce = false;
		var section = $('section.full_gallery_thumbs');
		$('section.full_gallery_thumbs .toggler').on('click', function() {
			if (toggler_debounce) {
				return false;
			}
			toggler_debounce = true;
			setTimeout(function() {
				toggler_debounce = false;
			}, 300);
			section.toggleClass('open');
		});
	})();

	function openPop(name) {
		if (popup_debounce) {
			return false;
		} 
		popup_debounce = true;
		setTimeout(function() {
			popup_debounce = false;
		}, 300);
		var overlay = $('#overlay');
		var pop = $('.pop.'+name);
		if (overlay.hasClass('show')) {
			popup_debounce = false;
			closePop();
			setTimeout(function() {
				openPop(name);
			}, 310);
			return false;
		}
		$('main').addClass('stop');
		pop.show(0, function() {
			if (name == 'book' && !$('section.full_gallery_thumbs').hasClass('activated')) {
				var mySwiper = new Swiper ('section.full_gallery_thumbs .main.swiper-container', {
					grabCursor: true,
					effect: 'coverflow',
					autoplay: {
						delay: 4000,
					},
					speed: 500,
					navigation: {
						nextEl: 'section.full_gallery_thumbs .swiper-button-next',
						prevEl: 'section.full_gallery_thumbs .swiper-button-prev',
					},
					on: {
						init: function () {
							setTimeout(function() {
								window.dispatchEvent(new Event('resize'));
							}, 300);
						}
					}
				});
			    var galleryThumbs = new Swiper('section.full_gallery_thumbs .gallery-thumbs', {
					spaceBetween: 16,
					centeredSlides: true,
					slidesPerView: 'auto',
					touchRatio: 0.2,
					slideToClickedSlide: true,
					on: {
						init: function () {
							setTimeout(function() {
								window.dispatchEvent(new Event('resize'));
							}, 300);
						}
					}
			    });
			    mySwiper.controller.control = galleryThumbs;
			    galleryThumbs.controller.control = mySwiper;
			    $('section.full_gallery_thumbs').addClass('activated');
			}
			overlay.addClass('show');
			pop.addClass('show');		
		});
	}

	function closePop() {
		if (popup_debounce) {
			return false;
		} 
		popup_debounce = true;
		setTimeout(function() {
			popup_debounce = false;
		}, 300);
		var overlay = $('#overlay');
		var pop = $('.pop');
		$('main').removeClass('stop');
		pop.removeClass('show');
		overlay.removeClass('show');
		setTimeout(function() {
			$('.pop .video_holder iframe').each(function(){
				var el_src = $(this).attr("src");
				$(this).attr("src",el_src);
			});
			pop.hide();
		}, 300);
	}

	function cart() {
		var empty = $('.cart_empty');
		var full = $('.cart_not_empty');
		var price_box = $('#price'); 
		var sum_box = $('#sum'); 
		var delivery_box = $('#delivery');

		function checkProducts() {
			var products = $('.product');
			if (products.length < 1) {
				empty.show();
				full.hide();
			}
		}
		checkProducts();
		function count() {
			var products = $('.product');
			var sum = 0;
			var delivery = parseInt(delivery_box.text());
			for (var i = 0, j = products.length; i < j; i++) {
				var product = products.eq(i);
				var quantity = parseInt(product.find('input').val());
				var price = parseInt(product.find('.price').text());
				sum += quantity * price;
			}
			price_box.text(sum + '€');
			sum_box.text( sum + delivery + '€');
		}
		count();
		$('input').on('blur', function() {
			var value = $(this).val();
			if (value == null || value < 0 || String(value).length == 0) {
				$(this).val(0);
			}
			count();
		}); 
		$('input').on('input', function() {
			count();
		}); 
		$('.plus').on('click', function() {
			var input = $(this).closest('.quantity').find('input');
			var value = parseInt(input.val());
			input.val(value + 1);
			count();
		});
		$('.minus').on('click', function() {
			var input = $(this).closest('.quantity').find('input');
			var value = parseInt(input.val());
			if (value > 0) {
				input.val(value - 1);
			}
			count();
		});
		$('.remove').on('click', function() {
			$(this).closest('.product').remove();
			checkProducts();
			count();
		});
	}

	function inputs() {
		var inputs = $('.simple_input input, .dropdown select');
		inputs.on('blur', function() {
			var value = String($(this).val());
			if (value.length > 0 && value != 'null') {
				$(this).addClass('focus');
			} else {
				$(this).removeClass('focus');
			}
		});
		inputs.on('focus', function() {
			$(this).removeClass('error');
			$(this).parent().find('.error_msg').slideUp(300);
		});
		$('input[type="checkbox"]').on('change', function() {
			$(this).removeClass('error');
		});
		inputs.trigger('blur');
	}

	function afterSubmit() {
		if (window.innerWidth <= 1000) {
			var main = $('main');
			var top = $('form .error').eq(0).offset().top + main.scrollTop() - 100;
			main.animate({
				scrollTop: top
			}, 300);
		}
	}

	(function() {
		if ($('.index_page').is(':visible')) {
			var scroll_debounce;
			var video_moved = false;
			var video = $('#author_video');
			var main = $('main');
			main.on('scroll', function(e) {
				clearTimeout(scroll_debounce);
				scroll_debounce = setTimeout(function() {
					var top = video.offset().top + main.scrollTop() - window.innerHeight;
					var scrolled = main.scrollTop();
					if (scrolled > top && video_moved == false) {
						video_moved = true;
						video.get(0).play();
					}
				}, 300);
			});
		}
	})();

})();