<?php include './partials/header.php';?>

	<div class="page news_page">
		<section class="text_heading">
			<div class="wrapper">
				<h1>
					Enimas minim veniam quis nostrud exercitation ullamco commodo consequat
					<div class="date">2018 spalio 15 d.</div>
				</h1>
			</div>
		</section>
		<img src="./media/images/future.jpg" class="full" alt="">
		<section class="two_columns">
			<div class="wrapper">
				<div class="heading">Enimas minim veniam quis nostrud exercitation ullamco commodo consequat</div>
				<div class="columns">
					Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore pariatur.<br/><br/>
					Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur? Quis autem vel eum iure reprehenderit.
					qui in ea voluptate velit esse quam nihil eu fugiat nulla molestiae consequatur.<br/><br/>
					Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore pariatur.<br/><br/>
					Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur? Quis autem vel eum iure reprehenderit.
					qui in ea voluptate velit esse quam nihil eu fugiat nulla molestiae consequatur.
				</div>
			</div>
		</section>
		<section class="two_images type_1">
			<div class="wrapper">
				<img src="./media/images/slide_1.jpg" alt="" class="img_1">
				<img src="./media/images/slide_2.jpg" alt="" class="img_2">
			</div>
		</section>
		<section class="three_columns">
			<div class="wrapper">
				<div class="heading">
					Ut enimad minveniam quino strud exercitation ullamco
				</div>
				<div class="columns">
					Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem nesciunt.<br/><br/>
					Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non.
					Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco commodo consequat.<br/><br/>
					Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Exce apteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit animest laborum. Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque. 
				</div>
			</div>
		</section>
	</div>
	
<?php include './partials/footer.php';?>
