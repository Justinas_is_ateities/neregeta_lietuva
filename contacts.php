<?php include './partials/header.php';?>

	<div class="page contacts_page">
		<section class="contacts">
			<div class="wrapper">
				<div class="left">
					<h1>Kontaktai</h1>
					<div class="contacts">
						<a href="https://www.google.lt/maps/place/A.+Go%C5%A1tauto+g.+40B,+Vilnius+01112/@54.685276,25.2567363,17z/data=!3m1!4b1!4m5!3m4!1s0x46dd940a5edc1707:0x465de1bb656ba6ee!8m2!3d54.685276!4d25.258925" target="_blank" class="address">
							A. Goštauto g. 40B, LT- 03163 Vilnius
						</a>
						<a class="phone" href="tel:+37061216590">+370 612 16590</a>
						<a class="email" href="mailto:marius@neregeta.lt">marius@neregeta.lt</a>
					</div>
					<div class="text">
						Norint įsigyti didesnį kiekį knygų, kreipkitės telefonu arba elektroniniu paštu.
					</div>
					<div class="socials">
						<a href="#" class="icon_facebook"></a>
						<a href="#" class="icon_instagram"></a>
					</div>
				</div>
			</div>
			<div id="map" class="right"></div>
		</section>
	</div>

	<script>
			function initMap() {
				var firstPin = {lat:  54.685276 , lng:  25.2567363 };

				var map = new google.maps.Map(document.getElementById('map'), {
					center: firstPin,
					zoom: 14,
					scrollwheel: false,
					gestureHandling: 'cooperative',
					styles: [
					    {
					        "featureType": "water",
					        "elementType": "geometry",
					        "stylers": [
					            {
					                "color": "#e9e9e9"
					            },
					            {
					                "lightness": 17
					            }
					        ]
					    },
					    {
					        "featureType": "landscape",
					        "elementType": "geometry",
					        "stylers": [
					            {
					                "color": "#f5f5f5"
					            },
					            {
					                "lightness": 20
					            }
					        ]
					    },
					    {
					        "featureType": "road.highway",
					        "elementType": "geometry.fill",
					        "stylers": [
					            {
					                "color": "#ffffff"
					            },
					            {
					                "lightness": 17
					            }
					        ]
					    },
					    {
					        "featureType": "road.highway",
					        "elementType": "geometry.stroke",
					        "stylers": [
					            {
					                "color": "#ffffff"
					            },
					            {
					                "lightness": 29
					            },
					            {
					                "weight": 0.2
					            }
					        ]
					    },
					    {
					        "featureType": "road.arterial",
					        "elementType": "geometry",
					        "stylers": [
					            {
					                "color": "#ffffff"
					            },
					            {
					                "lightness": 18
					            }
					        ]
					    },
					    {
					        "featureType": "road.local",
					        "elementType": "geometry",
					        "stylers": [
					            {
					                "color": "#ffffff"
					            },
					            {
					                "lightness": 16
					            }
					        ]
					    },
					    {
					        "featureType": "poi",
					        "elementType": "geometry",
					        "stylers": [
					            {
					                "color": "#f5f5f5"
					            },
					            {
					                "lightness": 21
					            }
					        ]
					    },
					    {
					        "featureType": "poi.park",
					        "elementType": "geometry",
					        "stylers": [
					            {
					                "color": "#dedede"
					            },
					            {
					                "lightness": 21
					            }
					        ]
					    },
					    {
					        "elementType": "labels.text.stroke",
					        "stylers": [
					            {
					                "visibility": "on"
					            },
					            {
					                "color": "#ffffff"
					            },
					            {
					                "lightness": 16
					            }
					        ]
					    },
					    {
					        "elementType": "labels.text.fill",
					        "stylers": [
					            {
					                "saturation": 36
					            },
					            {
					                "color": "#333333"
					            },
					            {
					                "lightness": 40
					            }
					        ]
					    },
					    {
					        "elementType": "labels.icon",
					        "stylers": [
					            {
					                "visibility": "off"
					            }
					        ]
					    },
					    {
					        "featureType": "transit",
					        "elementType": "geometry",
					        "stylers": [
					            {
					                "color": "#f2f2f2"
					            },
					            {
					                "lightness": 19
					            }
					        ]
					    },
					    {
					        "featureType": "administrative",
					        "elementType": "geometry.fill",
					        "stylers": [
					            {
					                "color": "#fefefe"
					            },
					            {
					                "lightness": 20
					            }
					        ]
					    },
					    {
					        "featureType": "administrative",
					        "elementType": "geometry.stroke",
					        "stylers": [
					            {
					                "color": "#fefefe"
					            },
					            {
					                "lightness": 17
					            },
					            {
					                "weight": 1.2
					            }
					        ]
					    }
					]
				});
				var marker = new google.maps.Marker({
					position: firstPin,
					map: map,
					icon: './media/images/pin.png'
		        });         
			}
		</script>
		<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyANOauHxcbjjK7eTa9Rjhj2mccAEXx3vnQ&callback=initMap"
		    async defer></script>

<?php include './partials/footer.php';?>