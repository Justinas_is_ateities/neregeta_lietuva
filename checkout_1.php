<?php include './partials/header.php';?>

	<div class="page dual_page checkout_page">
		<a href="#" class="logo">Neregėta Lietuva</a>
		<section class="steps">
			<div class="step">Krepšelis</div>
			<div class="step active">Informacija</div>
			<div class="step">Pristatymas</div>
			<div class="step">Apmokėjimas</div>
		</section>
		<form class="wrapper">
			<section class="checkout">
				<h2>Pirkėjo duomenys</h2>
				<div class="line">
					<div class="simple_input">
						<input type="text" name="name">
						<label>Vardas</label>
						<div class="error_msg">Lorem ipsum dolor</div>
					</div>
					<div class="simple_input">
						<input type="text" name="surname">
						<label>Pavardė</label>
						<div class="error_msg">Lorem ipsum dolor</div>
					</div>
				</div>
				<div class="line">
					<div class="simple_input">
						<input type="email" name="email">
						<label>El. paštas</label>
						<div class="error_msg">Lorem ipsum dolor</div>
					</div>
					<div class="simple_input">
						<input type="number" name="number">
						<label>Telefono numeris</label>
						<div class="error_msg">Lorem ipsum dolor</div>
					</div>
				</div>
				<label class="checkbox">
					<input type="checkbox" name="company">
					<span class="name">Perka įmonė</span>
				</label>
				<div class="company_toggler">
					<div class="line">
						<div class="simple_input">
							<input type="text" name="company_name">
							<label>Įmonės pavadinimas</label>
							<div class="error_msg">Lorem ipsum dolor</div>
						</div>
						<div class="simple_input">
							<input type="text" name="company_code">
							<label>Įmonės kodas</label>
							<div class="error_msg">Lorem ipsum dolor</div>
						</div>
					</div>
					<div class="line">
						<div class="simple_input">
							<input type="text" name="vat_number">
							<label>Pvm mokėtojo kodas</label>
							<div class="error_msg">Lorem ipsum dolor</div>
						</div>
					</div>
				</div>
				<div class="line">
					<div class="dropdown">
						<select name="country">
							<option disabled selected value=""></option>
							<option value="Lietuva">Lietuva</option>
							<option value="Rusija">Rusija</option>
							<option value="Lietuva">Lietuva</option>
							<option value="Rusija">Rusija</option>
							<option value="Lietuva">Lietuva</option>
							<option value="Rusija">Rusija</option>
						</select>
						<label>Šalis</label>
						<div class="error_msg">Lorem ipsum dolor</div>
					</div>
					<div class="simple_input">
						<input type="text" name="city">
						<label>Miestas</label>
						<div class="error_msg">Lorem ipsum dolor</div>
					</div>
				</div>
				<div class="line">
					<div class="simple_input">
						<input type="text" name="adrress">
						<label>Gatvė, namo numeris</label>
						<div class="error_msg">Lorem ipsum dolor</div>
					</div>
					<div class="simple_input with_link">
						<input type="text" name="post">
						<label>Pašto kodas</label>
						<a href="#" target="_blank">Kur rasti?</a>
						<div class="error_msg">Lorem ipsum dolor</div>
					</div>
				</div>
				<div class="simple_input">
					<input type="text" name="comment">
					<label>Pastabos</label>
					<div class="error_msg">Lorem ipsum dolor</div>
				</div>
				<div class="smaller_wrap">
					<div class="text">
						Jūsų asmeniniai duomenys bus naudojami užsakymo šioje svetainėje įvykdymui, taip pat kitiems tikslams, aprašytiems mūsų <a href="#">taisyklėse ir sąlygose</a>.
					</div>
					<label class="checkbox mb0">
						<input type="checkbox" name="rules">
						<span class="name">Su elektroninės parduotuvės <a href="#">taisyklėmis ir sąlygomis</a> susipažinau ir sutinku.</span>
					</label>
					<label class="checkbox">
						<input type="checkbox" name="register">
						<span class="name">Noriu sukurti paskyrą</span>
					</label>
					<div class="register_toggler">
						<div class="simple_input">
							<input type="password" name="password">
							<label>Slaptažodis</label>
							<div class="error_msg">Lorem ipsum dolor</div>
						</div>
						<div class="simple_input">
							<input type="password" name="re_password">
							<label>Patvirtinti slaptažodį</label>
							<div class="error_msg">Lorem ipsum dolor</div>
						</div>
						<div class="or">arba</div>
						<button type="submit" class="button blue fb"><span>Registruotis su facebook</span></button>
						<button type="submit" class="button blue google"><span>Registruotis su google+</span></button>
					</div>
				</div>
			</section>
			<section class="pay">
				<div class="content">
					<div class="line">
						<div class="label">Suma</div>
						<div class="price" id="price">200€</div>
					</div>
					<div class="line">
						<div class="label">Pristatymas</div>
						<div class="price" id="delivery">3€</div>
					</div>
					<div class="line main">
						<div class="label">Iš viso:</div>
						<div class="price" id="sum">203€</div>
					</div>
					<button type="submit" class="button blue"><span>Tęsti</span></button>
				</div>
			</section>
		</form>
	</div>

<?php include './partials/footer.php';?>