<?php include './partials/header.php';?>

	<div class="page solo_form_page">
		<form class="wrapper">
			<h2>Jūsų duomenys</h2>
			<div class="simple_input">
				<input type="text" name="name">
				<label>Vardas</label>
				<div class="error_msg">Lorem ipsum dolor</div>
			</div>
			<div class="simple_input">
				<input type="text" name="surname">
				<label>Pavardė</label>
				<div class="error_msg">Lorem ipsum dolor</div>
			</div>
			<div class="simple_input">
				<input type="email" name="email">
				<label>El. paštas</label>
				<div class="error_msg">Lorem ipsum dolor</div>
			</div>
			<div class="simple_input">
				<input type="number" name="number">
				<label>Telefono numeris</label>
				<div class="error_msg">Lorem ipsum dolor</div>
			</div>
			<h2>Jūsų adresas</h2>
			<div class="dropdown">
				<select name="country">
					<option disabled selected value=""></option>
					<option value="Lietuva">Lietuva</option>
					<option value="Rusija">Rusija</option>
					<option value="Lietuva">Lietuva</option>
					<option value="Rusija">Rusija</option>
					<option value="Lietuva">Lietuva</option>
					<option value="Rusija">Rusija</option>
				</select>
				<label>Šalis</label>
				<div class="error_msg">Lorem ipsum dolor</div>
			</div>
			<div class="simple_input">
				<input type="text" name="city">
				<label>Miestas</label>
				<div class="error_msg">Lorem ipsum dolor</div>
			</div>
			<div class="simple_input">
				<input type="text" name="adrress">
				<label>Gatvė, namo numeris</label>
				<div class="error_msg">Lorem ipsum dolor</div>
			</div>
			<div class="simple_input with_link">
				<input type="text" name="post">
				<label>Pašto kodas</label>
				<a href="#" target="_blank">Kur rasti?</a>
				<div class="error_msg">Lorem ipsum dolor</div>
			</div>
			<button type="submit" class="button blue with_states">
				<span class="simple">saugoti pakeitimus</span>
				<span class="success">Išsaugota</span>
			</button>
		</form>
	</div>

<?php include './partials/footer.php';?>