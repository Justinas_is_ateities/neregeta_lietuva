<?php include './partials/header.php';?>

	<div class="page black_page">
		<section class="black_hero">
			<div class="hero_video_background desktop_only"
				data-vide-bg="mp4: ./media/video/marius.mp4, poster: ./media/images/index_hero.jpg"
				data-vide-options="posterType: jpg, loop: true, muted: true, position: 50% 50%">
			</div>
			<div class="mobile_background mobile_only" style="background-image: url('./media/images/index_hero.jpg');"></div>
			<div class="wrapper">
				<div class="center">
					<h1>Paveldas</h1>
					<div class="excerpt">
						Magna aliquaut enim adminim veniam quis nostrud ullamco
					</div>
				</div>
			</div>
		</section>
		<section class="long_image">
			<div class="img desktop_only" style="background-image: url('./media/images/long.png');"></div>
			<div class="wrapper">
				<div class="texts">
					<div class="top_text">
						<h2>Lorem ipsum dolor sitam, consectetur adipiscing elit sed eiusmod tempor</h2>
						<div class="text">
							Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum lorem.
						</div>
					</div>
					<img class="mobile_only" src="./media/images/long.png"/>
					<div class="bottom_text">
						<h2>Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo</h2>
					</div>
				</div>
			</div>
		</section>
		<section class="half_picture">
			<img class="desktop_only" src="./media/images/egg.png" alt="">
			<img class="mobile_only" src="./media/images/egg_mobile.jpg" alt="">
			<div class="wrapper">
				<div class="left">
					<div class="center">
						<h2>Lorem ipsum dolor sitam, consectetur adipiscing elit sed eiusmod tempor</h2>
						<div class="text">
							Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip.
						</div>
					</div>
				</div>
			</div>
		</section>
		<section class="half_picture_reverse">
			<img src="./media/images/slide_3.jpg" alt="">
			<div class="wrapper">
				<div class="left">
					<div class="center">
						<h2>Lorem ipsum dolor sitam, consectetur adipiscing elit sed eiusmod tempor</h2>
						<div class="text">
							Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip.
						</div>
					</div>
				</div>
			</div>
		</section>
		<section class="book_options">
			<div class="wrapper">
				<div class="book_option">
					<div class="name">Nauja Neregėta Lietuva</div>
					<div class="price">79€</div>
					<a href="#" class="button blue"><span>Pirkti knygą</span></a>
				</div>
				<div class="book_option">
					<div class="name">Nauja Neregėta Lietuva</div>
					<div class="price">79€</div>
					<a href="#" class="button blue"><span>Pirkti knygą</span></a>
				</div>
				<div class="book_option">
					<div class="name">Nauja Neregėta Lietuva</div>
					<div class="price">79€</div>
					<a href="#" class="button blue"><span>Pirkti knygą</span></a>
				</div>
			</div>
		</section>
		<section class="categories">
			<div class="boxes">
				<a href="#" class="box" style="background-image: url('./media/images/slide_2.jpg');">
					<div class="name">PAVELDAS</div>
					<div class="content">
						Lorem ipsum dolorsit aconsect adipiscing elitsed do eiusmod tempor incididunt ut labore et dolore magna
					</div>
				</a>
				<a href="#" class="box" style="background-image: url('./media/images/slide_3.jpg');">
					<div class="name">ATEITIS</div>
					<div class="content">
						Lorem ipsum dolorsit aconsect adipiscing elitsed do eiusmod tempor incididunt ut labore et dolore magna
					</div>
				</a>
			</div>
		</section>
	</div>

<?php include './partials/footer.php';?>