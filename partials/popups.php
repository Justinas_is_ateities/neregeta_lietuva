<div id="overlay">
	<div class="pop book">
		<div class="close">Uždaryti</div>
		<section class="video">
			<div class="video_holder">
				<div class="poster" style="background-image: url('./media/images/video.jpg');"></div>
				<iframe src="https://www.youtube-nocookie.com/embed/SlbVgjFvE3I?rel=0" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
			</div>
		</section>
		<section class="heading">
			<div class="wrapper">
				Ut enim ad minim veniam quis nostrud exercitation ullamco
			</div>
		</section>
		<section class="two_full_images">
			<div class="left">
				<img src="./media/images/slide_1.jpg" alt="" class="left_img">	
			</div>
			<div class="right">
				<h2>Neque porro quisquam estqui dolorem ipsum</h2>
				<div class="text">
					Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae.
				</div>
				<img src="./media/images/slide_2.jpg" alt="">
			</div>
		</section>
		<section class="two_columns">
			<div class="wrapper">
				<div class="heading">Enimas minim veniam quis nostrud exercitation ullamco commodo consequat</div>
				<div class="columns">
					Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore pariatur.<br/><br/>
					Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur? Quis autem vel eum iure reprehenderit.
					qui in ea voluptate velit esse quam nihil eu fugiat nulla molestiae consequatur.<br/><br/>
					Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore pariatur.<br/><br/>
					Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur? Quis autem vel eum iure reprehenderit.
					qui in ea voluptate velit esse quam nihil eu fugiat nulla molestiae consequatur.
				</div>
			</div>
		</section>
		<section class="book_blocks">
			<div class="book_block">
				<div class="gallery_picture" style="background-image: url('./media/images/slide_3.jpg');"></div>
				<div class="wrapper">
					<div class="text_block">
						<div class="center">
							<h2>Nauja Neregėta Lietuva</h2>
							<div class="text">
								Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis.
							</div>
							<div class="extras">
								<div class="extra icon_weight">Svoris: 3.5 kg</div>
								<div class="extra icon_ruller">Matmenys: 226x331 mm</div>
								<div class="extra icon_size">Apimtis: 456 psl.</div>
								<div class="extra icon_isbn">ISBN 817525766-0</div>
							</div>
							<ul class="list">
								<li>Asmeninė autoriaus padėka knygoje</li>
								<li>Prabangi dovanų pakuotė</li>
								<li>Kvietimas į knygos pristatymą</li>
							</ul>
							<div class="button raw">Knygos pristatymas</div>
							<div class="price">79€</div>
							<a href="#" class="button blue"><span>Pirkti knygą</span></a>
						</div>
					</div>
				</div>
			</div>
		</section>
		<section class="full_gallery_thumbs open">
			<div class="swiper-container main">
				<div class="swiper-wrapper">
					<div class="swiper-slide" style="background-image: url('./media/images/slide_1.jpg');"></div>
					<div class="swiper-slide" style="background-image: url('./media/images/slide_2.jpg');"></div>
					<div class="swiper-slide" style="background-image: url('./media/images/slide_3.jpg');"></div>
					<div class="swiper-slide" style="background-image: url('./media/images/slide_1.jpg');"></div>
					<div class="swiper-slide" style="background-image: url('./media/images/slide_2.jpg');"></div>
					<div class="swiper-slide" style="background-image: url('./media/images/slide_3.jpg');"></div>
				</div>
				<div class="swiper-button-next icon_arrow_right"></div>
				<div class="swiper-button-prev icon_arrow_left"></div>
			</div>
			<div class="swiper-container gallery-thumbs">
				<div class="swiper-wrapper">
					<div class="swiper-slide" style="background-image: url('./media/images/slide_3.jpg');"></div>
					<div class="swiper-slide" style="background-image: url('./media/images/slide_2.jpg');"></div>
					<div class="swiper-slide" style="background-image: url('./media/images/slide_1.jpg');"></div>
					<div class="swiper-slide" style="background-image: url('./media/images/slide_2.jpg');"></div>
					<div class="swiper-slide" style="background-image: url('./media/images/slide_3.jpg');"></div>
					<div class="swiper-slide" style="background-image: url('./media/images/slide_1.jpg');"></div>
				</div>
			</div>
			<div class="toggler">
				<span class="more">Daugiau</span>
				<span class="less">Mažiau</span>
			</div>
		</section>
		<section class="book_options">
			<div class="wrapper">
				<div class="book_option">
					<img src="./media/images/book.jpg" alt="">
					<div class="name">Nauja Neregėta Lietuva</div>
					<div class="price">79€</div>
					<a href="#" class="button blue"><span>Pirkti knygą</span></a>
				</div>
			</div>
		</section>
	</div>
	<div class="pop buy">
		<div class="close">Uždaryti</div>
		<section class="books">
			<div class="wrapper">
				<div class="book_block">
					<div class="img_holder">
						<img src="./media/images/book.jpg" alt="">
						<div class="pattern" data-pop-book>
							<div class="center">
								<div class="icon"></div>
								<div class="text">knygos pristatymas</div>
							</div>
						</div>
					</div>
					<div class="title">Nauja Neregėta Lietuva</div>
					<div class="about">
						Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore.
					</div>
					<div class="langs">
						Kalba: Lietuvių
					</div>
					<div class="price">79€</div>
					<a href="#" class="button blue"><span>Pirkti knygą</span></a>
				</div>
				<div class="book_block">
					<div class="img_holder">
						<img src="./media/images/book.jpg" alt="">
						<div class="pattern" data-pop-book>
							<div class="center">
								<div class="icon"></div>
								<div class="text">knygos pristatymas</div>
							</div>
						</div>
					</div>
					<div class="title">Nauja Neregėta Lietuva</div>
					<div class="about">
						Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore.
					</div>
					<div class="langs">
						Kalba: Lietuvių
					</div>
					<div class="price">79€</div>
					<a href="#" class="button blue"><span>Pirkti knygą</span></a>
				</div>
				<div class="book_block">
					<div class="img_holder">
						<img src="./media/images/book.jpg" alt="">
						<div class="pattern" data-pop-book>
							<div class="center">
								<div class="icon"></div>
								<div class="text">knygos pristatymas</div>
							</div>
						</div>
					</div>
					<div class="title">Nauja Neregėta Lietuva</div>
					<div class="about">
						Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore.
					</div>
					<div class="langs">
						Kalba: Lietuvių
					</div>
					<div class="price">79€</div>
					<a href="#" class="button blue"><span>Pirkti knygą</span></a>
				</div>
				<div class="book_block">
					<div class="img_holder">
						<img src="./media/images/book.jpg" alt="">
						<div class="pattern" data-pop-book>
							<div class="center">
								<div class="icon"></div>
								<div class="text">knygos pristatymas</div>
							</div>
						</div>
					</div>
					<div class="title">Nauja Neregėta Lietuva</div>
					<div class="about">
						Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore.
					</div>
					<div class="langs">
						Kalba: Lietuvių
					</div>
					<div class="price">79€</div>
					<a href="#" class="button blue"><span>Pirkti knygą</span></a>
				</div>
				<div class="book_block">
					<div class="img_holder">
						<img src="./media/images/book.jpg" alt="">
						<div class="pattern" data-pop-book>
							<div class="center">
								<div class="icon"></div>
								<div class="text">knygos pristatymas</div>
							</div>
						</div>
					</div>
					<div class="title">Nauja Neregėta Lietuva</div>
					<div class="about">
						Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore.
					</div>
					<div class="langs">
						Kalba: Lietuvių
					</div>
					<div class="price">79€</div>
					<a href="#" class="button blue"><span>Pirkti knygą</span></a>
				</div>
				<div class="book_block">
					<div class="img_holder">
						<img src="./media/images/book.jpg" alt="">
						<div class="pattern" data-pop-book>
							<div class="center">
								<div class="icon"></div>
								<div class="text">knygos pristatymas</div>
							</div>
						</div>
					</div>
					<div class="title">Nauja Neregėta Lietuva</div>
					<div class="about">
						Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore.
					</div>
					<div class="langs">
						Kalba: Lietuvių
					</div>
					<div class="price">79€</div>
					<a href="#" class="button blue"><span>Pirkti knygą</span></a>
				</div>
			</div>
		</section>
	</div>
	<div class="pop add">
		<div class="close">Uždaryti</div>
		<section class="pop_add_content" style="background-image: url('./media/images/pop_add.jpg');">
			<div class="wrapper">
				<h1>Lorem ipsum dolor sit amet consectetur adipiscing elit</h1>
				<div class="icons">
					<div class="date">2018 spalio 15 d.</div>
					<div class="time">17:30 val.</div>
				</div>
				<div class="button blue"><span>Sužinokite daugiau</span></div>
			</div>
		</section>
	</div>
</div>