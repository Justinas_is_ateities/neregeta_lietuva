<!DOCTYPE HTML>
<html>
	<head>
		<title>Neregėta Lietuva</title>	
		<meta charset="UTF-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimum-scale=1.0, maximum-scale=1.0">
		<link rel="stylesheet" type="text/css" media="screen" href="css/main.css" />
	</head>
	<body>

		<header>
			<div class="wrapper">
				<div class="logo">Neregėta lietuva</div>
				<div class="mobile_menu_controls">
					<a href="#" class="cart"><span>1</span></a>
					<div class="person">
						<div class="selector_holder">
							<div class="content">
								<a href="#">Prisijungti</a>
								<a href="#">Registruotis</a>
							</div>
						</div>
					</div>
					<div class="lang_selector">
						<div class="current">LT</div>
						<div class="selector_holder">
							<div class="content">
								<a href="#">RU</a>
								<a href="#">EN</a>
							</div>
						</div>
					</div>
				</div>
				<div class="fr">
					<div data-pop-buy class="button blue"><span>Pirkti knygą</span></div>
					<a href="#" class="cart"><span>1</span></a>
					<div class="person">
						<div class="selector_holder">
							<div class="content">
								<a href="#">Prisijungti</a>
								<a href="#">Registruotis</a>
							</div>
						</div>
					</div>
					<div class="lang_selector">
						<div class="current">LT</div>
						<div class="selector_holder">
							<div class="content">
								<a href="#">RU</a>
								<a href="#">EN</a>
							</div>
						</div>
					</div>
					<div class="burger">
						<span>
							<span>meniu</span>
							<span>atgal</span>
						</span>
						<div class="box">
							<div class="plank"></div>
							<div class="plank"></div>
							<div class="plank"></div>
							<div class="plank"></div>
							<div class="plank"></div>
						</div>
					</div>
				</div>
			</div>
		</header>

		<section class="menu">
			<div class="wrapper">
				<div class="holder"></div>
				<div class="overlay"></div>
				<div class="content">
					<div class="center">					
						<a href="#">Naujoji knyga</a>
						<a href="#" class="small">Kraštovaizdis</a>
						<a href="#" class="small">Paveldas</a>
						<a href="#" class="small mb15">Ateitis</a>
						<a href="#">Neregėtos Lietuvos kelias</a>
						<a href="#">Apie knygą kalba</a>
						<a href="#">Naujienos</a>
						<a href="#">Kontaktai</a>
						<div data-pop-buy class="button blue"><span>Pirkti knygą</span></div>
						<div class="socials">
							<a href="#" class="icon_facebook"></a>
							<a href="#" class="icon_instagram"></a>
						</div>
					</div>
				</div>
				<div class="clear"></div>
			</div>
		</section>
		
		<main>
			<div id="scroller">
