
				<footer>
					<div class="wrapper">
            <div class="column copy">
              <div>© Visos teisės saugomos 2018 Neregėta Lietuva.</div>
              <div><a href="#">Privatumo politika</a></div>
            </div>
            <div class="column middle">
              <div>UAB „Unseen Pictures”</div>
              <div>
                <a href="https://www.google.lt/maps/place/A.+Go%C5%A1tauto+g.+40B,+Vilnius+01112/@54.685276,25.2567363,17z/data=!3m1!4b1!4m5!3m4!1s0x46dd940a5edc1707:0x465de1bb656ba6ee!8m2!3d54.685276!4d25.258925" target="_blank" class="address">
                  A. Goštauto g. 40B,LT- 03163 Vilnius
                </a>
              </div>
            </div>
            <div class="column last">
              <div>Tel. nr.: <a class="phone" href="tel:+37061216590">+370 612 16590</a></div>
              <div>El. paštas: <a class="email" href="mailto:marius@neregeta.lt">marius@neregeta.lt</a></div>
            </div>
					</div>
				</footer>
			</div>
		</main>

    <div class="cookies_box">
      <div class="wrapper">
        <div class="text">
          Curabitur pharetra venenatis eros, a lobortis quam aliquam sit amet. Mauris bibendum dictum ante, a porta erat viverra non. Vivamus laoreet semper gravida. Fusce semper est felis, quis egestas purus malesuada vel. Cras laoreet blandit ligula laoreet ultrices.
        </div>
        <div class="button blue"><span>Sutinku</span></div>
      </div>
    </div>

		<?php include './partials/popups.php';?>

		<style>
  #mmm{
      position:fixed;
      width:300px;
      right:-310px;
      width:300px;
      top:0;
      background: #1a1b1f;
      z-index:200;
      padding:15px;
      top:100px;
  }
  #mmm:hover{
      right:0;
  }
  #mmm a{
      text-decoration:none;
      color:#fff;
      font-size:12px;
      display:block;
  }
  #ooo{
      width:44px;
      height:34px;
      position:absolute;
      top:0;
      left:-44px;
      background: #1a1b1f;
      border: 0px solid #000000;
      cursor: pointer;
  }
</style>

    <div id="mmm">
        <div id="ooo"></div>
      <?php
      $files = scandir(getcwd());
      foreach($files as $k => $file){
          if (strpos($file,'.php') !== false)
              echo '<a href="'.$file.'">'.$file.'</a>';
      }
      ?>
    </div>
		
		<script type="text/javascript" src="./js/libs.js"></script>
    	<script type="text/javascript" src="./js/script.js"></script>
	</body>
</html>