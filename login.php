<?php include './partials/header.php';?>

	<div class="page dual_page login_page">
		<h1 class="wrapper">Prisijunkite</h1>
		<div class="wrapper">
			<form class="login left">
				<h2>Grįžtantis pirkėjas</h2>
				<div class="simple_input">
					<input type="email" name="email">
					<label>El. paštas</label>
					<div class="error_msg">Lorem ipsum dolor</div>
				</div>
				<div class="simple_input">
					<input type="password" name="password">
					<label>Slaptažodis</label>
					<div class="error_msg">Lorem ipsum dolor</div>
				</div>
				<button type="submit" class="button blue"><span>Prisijungti</span></button>
				<div class="line">
					<label class="checkbox">
						<input type="checkbox" name="remember">
						<span class="name">Prisiminti mane</span>
					</label>
					<a href="#">Priminti slaptažodį</a>
				</div>
				<button type="submit" class="button blue fb"><span>Prisijungti su facebook</span></button>
				<button type="submit" class="button blue google"><span>Prisijungti su google+</span></button>
			</form>
			<div class="right">
				<h2>Naujas pirkėjas</h2>
				<a href="#" class="button ghost"><span>REGISTRUOTIS</span></a>
				<div class="or">arba</div>
				<a href="#" class="button ghost"><span>TĘSTI NEPRISIREGISTRAVUS</span></a>
			</div>
		</div>
	</div>

<?php include './partials/footer.php';?>