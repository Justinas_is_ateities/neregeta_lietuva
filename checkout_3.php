<?php include './partials/header.php';?>

	<div class="page dual_page checkout_page">
		<a href="#" class="logo">Neregėta Lietuva</a>
		<section class="steps">
			<div class="step">Krepšelis</div>
			<div class="step">Informacija</div>
			<div class="step">Pristatymas</div>
			<div class="step active">Apmokėjimas</div>
		</section>
		<form class="wrapper">
			<section class="checkout">
				<h2>Apmokėjimo būdas</h2>
				<div class="pay_type">
					Elektroninė bankininkystė
				</div>
				<div class="pay_selection">
					<label class="radio">
						<input type="radio" name='pay_type' value="swedbank" checked>
						<span class="name">
							<span class="bank_name">Swedbank</span>
							<span class="img_holder">
								<img src="./media/images/swedbank.png" alt="">
							</span>
						</span>
					</label>
					<label class="radio">
						<input type="radio" name='pay_type' value="citadele">
						<span class="name">
							<span class="bank_name">Citadele bankas</span>
							<span class="img_holder">
								<img src="./media/images/citadele.png" alt="">
							</span>
						</span>
					</label>
					<label class="radio">
						<input type="radio" name='pay_type' value="swedbank" checked>
						<span class="name">
							<span class="bank_name">Swedbank</span>
							<span class="img_holder">
								<img src="./media/images/swedbank.png" alt="">
							</span>
						</span>
					</label>
					<label class="radio">
						<input type="radio" name='pay_type' value="citadele">
						<span class="name">
							<span class="bank_name">Citadele bankas</span>
							<span class="img_holder">
								<img src="./media/images/citadele.png" alt="">
							</span>
						</span>
					</label>
					<label class="radio">
						<input type="radio" name='pay_type' value="swedbank" checked>
						<span class="name">
							<span class="bank_name">Swedbank</span>
							<span class="img_holder">
								<img src="./media/images/swedbank.png" alt="">
							</span>
						</span>
					</label>
					<label class="radio">
						<input type="radio" name='pay_type' value="citadele">
						<span class="name">
							<span class="bank_name">Citadele bankas</span>
							<span class="img_holder">
								<img src="./media/images/citadele.png" alt="">
							</span>
						</span>
					</label>
				</div>
				<div class="pay_type">
					Elektroninė bankininkystė
				</div>
				<div class="pay_selection">
					<label class="radio">
						<input type="radio" name='pay_type' value="swedbank">
						<span class="name">
							<span class="bank_name">Swedbank</span>
							<span class="img_holder">
								<img src="./media/images/swedbank.png" alt="">
							</span>
						</span>
					</label>
					<label class="radio">
						<input type="radio" name='pay_type' value="citadele">
						<span class="name">
							<span class="bank_name">Citadele bankas</span>
							<span class="img_holder">
								<img src="./media/images/citadele.png" alt="">
							</span>
						</span>
					</label>
				</div>
			</section>
			<section class="pay">
				<div class="content">
					<div class="line">
						<div class="label">Suma</div>
						<div class="price" id="price">200€</div>
					</div>
					<div class="line">
						<div class="label">Pristatymas</div>
						<div class="price" id="delivery">3€</div>
					</div>
					<div class="line main">
						<div class="label">Iš viso:</div>
						<div class="price" id="sum">203€</div>
					</div>
					<button type="submit" class="button blue"><span>Apmokėti</span></button>
				</div>
			</section>
		</form>
	</div>

<?php include './partials/footer.php';?>