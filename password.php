<?php include './partials/header.php';?>

	<div class="page solo_form_page">
		<form class="wrapper">
			<h2>Keisti slaptažodį</h2>
			<div class="simple_input">
				<input type="password" name="current_password">
				<label>Dabartinis slaptažodis</label>
				<div class="error_msg">Lorem ipsum dolor</div>
			</div>
			<div class="simple_input">
				<input type="password" name="new_password">
				<label>Naujas slaptažodis</label>
				<div class="error_msg">Lorem ipsum dolor</div>
			</div>
			<div class="simple_input">
				<input type="password" name="re_new_password">
				<label>Pakartoti naują slaptažodį</label>
				<div class="error_msg">Lorem ipsum dolor</div>
			</div>

			<button type="submit" class="button blue"><span>saugoti pakeitimus</span></button>
		</form>
	</div>

<?php include './partials/footer.php';?>