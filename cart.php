<?php include './partials/header.php';?>

	<div class="page dual_page cart_page">
		<h1 class="wrapper cart_not_empty">Jūsų krepšelis</h1>
		<div class="wrapper cart_empty">Jūsų krepšelis tuščias</div>
		<div class="wrapper cart_not_empty">
			<section class="cart">
				<div class="product">
					<img src="./media/images/book.jpg" alt="">
					<div class="name">Nauja Neregėta Lietuva</div>
					<div class="quantity">
						<div class="minus">-</div>
						<input type="number" min="0" value="1">
						<div class="plus">+</div>
					</div>
					<div class="price">79€</div>
					<div class="remove"></div>
				</div>
				<div class="product">
					<img src="./media/images/book.jpg" alt="">
					<div class="name">Nauja Neregėta Lietuva</div>
					<div class="quantity">
						<div class="minus">-</div>
						<input type="number" min="0" value="1">
						<div class="plus">+</div>
					</div>
					<div class="price">79€</div>
					<div class="remove"></div>
				</div>
				<div class="product">
					<img src="./media/images/book.jpg" alt="">
					<div class="name">Nauja Neregėta Lietuva</div>
					<div class="quantity">
						<div class="minus">-</div>
						<input type="number" min="0" value="1">
						<div class="plus">+</div>
					</div>
					<div class="price">79€</div>
					<div class="remove"></div>
				</div>
				<div class="product">
					<img src="./media/images/book.jpg" alt="">
					<div class="name">Nauja Neregėta Lietuva</div>
					<div class="quantity">
						<div class="minus">-</div>
						<input type="number" min="0" value="1">
						<div class="plus">+</div>
					</div>
					<div class="price">79€</div>
					<div class="remove"></div>
				</div>
				<div class="product">
					<img src="./media/images/book.jpg" alt="">
					<div class="name">Nauja Neregėta Lietuva</div>
					<div class="quantity">
						<div class="minus">-</div>
						<input type="number" min="0" value="1">
						<div class="plus">+</div>
					</div>
					<div class="price">79€</div>
					<div class="remove"></div>
				</div>
				<div class="product">
					<img src="./media/images/book.jpg" alt="">
					<div class="name">Nauja Neregėta Lietuva</div>
					<div class="quantity">
						<div class="minus">-</div>
						<input type="number" min="0" value="1">
						<div class="plus">+</div>
					</div>
					<div class="price">79€</div>
					<div class="remove"></div>
				</div>
				<div class="product">
					<img src="./media/images/book.jpg" alt="">
					<div class="name">Nauja Neregėta Lietuva</div>
					<div class="quantity">
						<div class="minus">-</div>
						<input type="number" min="0" value="1">
						<div class="plus">+</div>
					</div>
					<div class="price">79€</div>
					<div class="remove"></div>
				</div>
				<div class="product">
					<img src="./media/images/book.jpg" alt="">
					<div class="name">Nauja Neregėta Lietuva</div>
					<div class="quantity">
						<div class="minus">-</div>
						<input type="number" min="0" value="1">
						<div class="plus">+</div>
					</div>
					<div class="price">79€</div>
					<div class="remove"></div>
				</div>
			</section>
			<section class="pay">
				<div class="content">
					<div class="line">
						<div class="label">Suma</div>
						<div class="price" id="price"></div>
					</div>
					<div class="line">
						<div class="label">Pristatymas</div>
						<div class="price" id="delivery">3€</div>
					</div>
					<div class="line main">
						<div class="label">Iš viso:</div>
						<div class="price" id="sum"></div>
					</div>
					<a href="#" class="button blue"><span>Apmokėti</span></a>
					<a href="#" class="button raw"><span>Tęsti apsipirkimą</span></a>
				</div>
			</section>
		</div>
	</div>

<?php include './partials/footer.php';?>