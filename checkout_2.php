<?php include './partials/header.php';?>

	<div class="page dual_page checkout_page">
		<a href="#" class="logo">Neregėta Lietuva</a>
		<section class="steps">
			<div class="step">Krepšelis</div>
			<div class="step">Informacija</div>
			<div class="step active">Pristatymas</div>
			<div class="step">Apmokėjimas</div>
		</section>
		<form class="wrapper">
			<section class="checkout">
				<h2>Pristatymo būdas</h2>
				<div class="line delivery_line">
					<label class="delivery_type">
						<input type="radio" name="delivery_type" value="dpd" checked>
						<span class="content">
							<span class="img_holder">
								<img src="./media/images/dpd.png" alt="">
							</span>
							<span class="price">3,10 €</span>
							<span class="comment">2-4 darbo dienos</span>
						</span>
					</label>
					<label class="delivery_type">
						<input type="radio" name="delivery_type" value="lp">
						<span class="content">
							<span class="img_holder">
								<img src="./media/images/lp.png" alt="">
							</span>
							<span class="price">3,10 €</span>
							<span class="comment">2-4 darbo dienos</span>
						</span>
					</label>
					<label class="delivery_type">
						<input type="radio" name="delivery_type" value="dpd_2">
						<span class="content">
							<span class="img_holder">
								<img src="./media/images/dpd.png" alt="">
							</span>
							<span class="price">3,10 €</span>
							<span class="comment">2-4 darbo dienos</span>
						</span>
					</label>
					<label class="delivery_type">
						<input type="radio" name="delivery_type" value="lp_2">
						<span class="content">
							<span class="img_holder">
								<img src="./media/images/lp.png" alt="">
							</span>
							<span class="price">3,10 €</span>
							<span class="comment">2-4 darbo dienos</span>
						</span>
					</label>
				</div>
			</section>
			<section class="pay">
				<div class="content">
					<div class="line">
						<div class="label">Suma</div>
						<div class="price" id="price">200€</div>
					</div>
					<div class="line">
						<div class="label">Pristatymas</div>
						<div class="price" id="delivery">3€</div>
					</div>
					<div class="line main">
						<div class="label">Iš viso:</div>
						<div class="price" id="sum">203€</div>
					</div>
					<button type="submit" class="button blue"><span>Tęsti</span></button>
				</div>
			</section>
		</form>
	</div>

<?php include './partials/footer.php';?>