<?php include './partials/header.php';?>

	<div class="page black_page">
		<section class="black_hero">
			<div class="hero_video_background desktop_only"
				data-vide-bg="mp4: ./media/video/marius.mp4, poster: ./media/images/index_hero.jpg"
				data-vide-options="posterType: jpg, loop: true, muted: true, position: 50% 50%">
			</div>
			<div class="mobile_background mobile_only" style="background-image: url('./media/images/index_hero.jpg');"></div>
			<div class="wrapper">
				<div class="center">
					<h1>Ateitis</h1>
					<div class="excerpt">
						Magna aliquaut enim adminim veniam quis nostrud ullamco
					</div>
				</div>
			</div>
		</section>
		<section class="picture_text">
			<img src="./media/images/future_green.jpg" alt="">
			<img src="./media/images/future_blue.jpg" alt="" class="right_img_mobile mobile_only">
			<div class="wrapper">
				<div class="left">
					<div class="center">
						<h2>Lorem ipsum dolor sitam, consectetur adipiscing elit sed eiusmod tempor</h2>
						<div class="text">
							Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip.
						</div>
					</div>
				</div>
			</div>
			<img src="./media/images/future_blue.jpg" alt="" class="right_img desktop_only">
		</section>
		<section class="text_image type_4">
			<div class="img_1" style="background-image: url('./media/images/slide_3.jpg');"></div>
			<div class="wrapper">
				<div class="text_block">
					<h2>Lorem ipsum dolor sitam, consectetur adipiscing elit sed eiusmod tempor </h2>
					<div class="text">
						Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi commodo consequat.
					</div>
				</div>
			</div>
		</section>
		<section class="full_gallery swiper-container">
			<div class="swiper-wrapper">
				<div class="swiper-slide" style="background-image: url('./media/images/slide_1.jpg');">
					<div class="heading">Enimas minim veniam quis nostrud exercitation ullamco commodo consequat</div>
				</div>
				<div class="swiper-slide" style="background-image: url('./media/images/slide_2.jpg');">
					<div class="heading">Enimas minim veniam quis nostrud exercitation ullamco commodo consequat</div>
				</div>
				<div class="swiper-slide" style="background-image: url('./media/images/slide_3.jpg');">
					<div class="heading">Enimas minim veniam quis nostrud exercitation ullamco commodo consequat</div>
				</div>
			</div>
			<div class="swiper-button-next icon_arrow_right"></div>
			<div class="swiper-button-prev icon_arrow_left"></div>
		</section>
		<section class="two_images type_2">
			<div class="wrapper">
				<div class="left">
					<img src="./media/images/slide_1.jpg">
				</div>
				<div class="right">
					<h2>Ut enim ad minim veniam, quis nostrud exercitation ullamco</h2>
					<img src="./media/images/slide_2.jpg" alt="">
				</div>
			</div>
		</section>
		<section class="book_options">
			<div class="wrapper">
				<div class="book_option">
					<div class="name">Nauja Neregėta Lietuva</div>
					<div class="price">79€</div>
					<a href="#" class="button blue"><span>Pirkti knygą</span></a>
				</div>
				<div class="book_option">
					<div class="name">Nauja Neregėta Lietuva</div>
					<div class="price">79€</div>
					<a href="#" class="button blue"><span>Pirkti knygą</span></a>
				</div>
				<div class="book_option">
					<div class="name">Nauja Neregėta Lietuva</div>
					<div class="price">79€</div>
					<a href="#" class="button blue"><span>Pirkti knygą</span></a>
				</div>
			</div>
		</section>
		<section class="categories">
			<div class="boxes">
				<a href="#" class="box" style="background-image: url('./media/images/slide_2.jpg');">
					<div class="name">KRAŠTOVAIZDIS</div>
					<div class="content">
						Lorem ipsum dolorsit aconsect adipiscing elitsed do eiusmod tempor incididunt ut labore et dolore magna
					</div>
				</a>
				<a href="#" class="box" style="background-image: url('./media/images/slide_3.jpg');">
					<div class="name">PAVELDAS</div>
					<div class="content">
						Lorem ipsum dolorsit aconsect adipiscing elitsed do eiusmod tempor incididunt ut labore et dolore magna
					</div>
				</a>
			</div>
		</section>
	</div>

<?php include './partials/footer.php';?>