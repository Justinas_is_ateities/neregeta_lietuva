<?php include './partials/header.php';?>

	<div class="page book_page">
		<section class="text_heading">
			<div class="wrapper">
				<h1>Enimas minim veniam quis nostrud exercitation ullamco commodo consequat</h1>
			</div>
		</section>
		<section class="book_blocks">
			<div class="book_block">
				<div class="gallery swiper-container">
					<div class="swiper-wrapper">
						<div class="swiper-slide" style="background-image: url('./media/images/slide_1.jpg');"></div>
						<div class="swiper-slide" style="background-image: url('./media/images/slide_2.jpg');"></div>
						<div class="swiper-slide" style="background-image: url('./media/images/slide_3.jpg');"></div>
					</div>
					<div class="swiper-button-next icon_arrow_right"></div>
					<div class="swiper-button-prev icon_arrow_left"></div>
				</div>
				<div class="wrapper">
					<div class="text_block">
						<div class="center">
							<h2>Nauja Neregėta Lietuva</h2>
							<div class="text">
								Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis.
							</div>
							<div class="extras">
								<div class="extra icon_weight">Svoris: 3.5 kg</div>
								<div class="extra icon_ruller">Matmenys: 226x331 mm</div>
								<div class="extra icon_size">Apimtis: 456 psl.</div>
								<div class="extra icon_isbn">ISBN 817525766-0</div>
							</div>
							<ul class="list">
								<li>Asmeninė autoriaus padėka knygoje</li>
								<li>Prabangi dovanų pakuotė</li>
								<li>Kvietimas į knygos pristatymą</li>
							</ul>
							<div class="button raw">Knygos pristatymas</div>
							<div class="price">79€</div>
							<a href="#" class="button blue"><span>Pirkti knygą</span></a>
						</div>
					</div>
				</div>
			</div>
			<div class="book_block">
				<div class="gallery swiper-container">
					<div class="swiper-wrapper">
						<div class="swiper-slide" style="background-image: url('./media/images/slide_1.jpg');"></div>
						<div class="swiper-slide" style="background-image: url('./media/images/slide_2.jpg');"></div>
						<div class="swiper-slide" style="background-image: url('./media/images/slide_3.jpg');"></div>
					</div>
					<div class="swiper-button-next icon_arrow_right"></div>
					<div class="swiper-button-prev icon_arrow_left"></div>
				</div>
				<div class="wrapper">
					<div class="text_block">
						<div class="center">
							<h2>Nauja Neregėta Lietuva</h2>
							<div class="text">
								Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis.
							</div>
							<div class="extras">
								<div class="extra icon_weight">Svoris: 3.5 kg</div>
								<div class="extra icon_ruller">Matmenys: 226x331 mm</div>
								<div class="extra icon_size">Apimtis: 456 psl.</div>
								<div class="extra icon_isbn">ISBN 817525766-0</div>
							</div>
							<ul class="list">
								<li>Asmeninė autoriaus padėka knygoje</li>
								<li>Prabangi dovanų pakuotė</li>
								<li>Kvietimas į knygos pristatymą</li>
							</ul>
							<div class="button raw">Knygos pristatymas</div>
							<div class="price">79€</div>
							<a href="#" class="button blue"><span>Pirkti knygą</span></a>
						</div>
					</div>
				</div>
			</div>
			<div class="book_block">
				<div class="gallery swiper-container">
					<div class="swiper-wrapper">
						<div class="swiper-slide" style="background-image: url('./media/images/slide_1.jpg');"></div>
						<div class="swiper-slide" style="background-image: url('./media/images/slide_2.jpg');"></div>
						<div class="swiper-slide" style="background-image: url('./media/images/slide_3.jpg');"></div>
					</div>
					<div class="swiper-button-next icon_arrow_right"></div>
					<div class="swiper-button-prev icon_arrow_left"></div>
				</div>
				<div class="wrapper">
					<div class="text_block">
						<div class="center">
							<h2>Nauja Neregėta Lietuva</h2>
							<div class="text">
								Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis.
							</div>
							<div class="extras">
								<div class="extra icon_weight">Svoris: 3.5 kg</div>
								<div class="extra icon_ruller">Matmenys: 226x331 mm</div>
								<div class="extra icon_size">Apimtis: 456 psl.</div>
								<div class="extra icon_isbn">ISBN 817525766-0</div>
							</div>
							<ul class="list">
								<li>Asmeninė autoriaus padėka knygoje</li>
								<li>Prabangi dovanų pakuotė</li>
								<li>Kvietimas į knygos pristatymą</li>
							</ul>
							<div class="button raw">Knygos pristatymas</div>
							<div class="price">79€</div>
							<a href="#" class="button blue"><span>Pirkti knygą</span></a>
						</div>
					</div>
				</div>
			</div>
		</section>
		<section class="heading">
			<div class="wrapper">
				Ut enim ad minim veniam quis nostrud exercitation ullamco labis utaliquip exea lorem 
			</div>
		</section>
		<section class="text_image type_2">
			<div class="img_1" style="background-image: url('./media/images/slide_3.jpg');"></div>
			<div class="wrapper">
				<div class="text_block">
					<h2>Neque porro quisquam estqui dolorem ipsum quia dolor sit</h2>
					<div class="text">
						Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi numquam eius modi voluptas sit aspernatur nesciunt.
					</div>
					<img src="./media/images/slide_3.jpg" alt=""/>
				</div>
			</div>
		</section>
		<section class="two_columns">
			<div class="wrapper">
				<div class="heading">Enimas minim veniam quis nostrud exercitation ullamco commodo consequat</div>
				<div class="columns">
					Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore pariatur.<br/><br/>
					Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur? Quis autem vel eum iure reprehenderit.
					qui in ea voluptate velit esse quam nihil eu fugiat nulla molestiae consequatur.<br/><br/>
					Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore pariatur.<br/><br/>
					Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur? Quis autem vel eum iure reprehenderit.
					qui in ea voluptate velit esse quam nihil eu fugiat nulla molestiae consequatur.
				</div>
			</div>
		</section>
		<section class="video">
			<div class="video_holder">
				<div class="poster" style="background-image: url('./media/images/video.jpg');"></div>
				<iframe src="https://www.youtube-nocookie.com/embed/SlbVgjFvE3I?rel=0" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
			</div>
		</section>
		<section class="heading">
			<div class="wrapper">
				Ut enim ad minim veniam quis nostrud exercitation ullamco laboris aliquip
			</div>
		</section>
		<section class="two_images type_1">
			<div class="wrapper">
				<img src="./media/images/slide_1.jpg" alt="" class="img_1">
				<img src="./media/images/slide_2.jpg" alt="" class="img_2">
			</div>
		</section>
		<section class="three_columns">
			<div class="wrapper">
				<div class="heading">
					Ut enimad minveniam quino strud exercitation ullamco
				</div>
				<div class="columns">
					Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem nesciunt.<br/><br/>
					Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non.
					Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco commodo consequat.<br/><br/>
					Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Exce apteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit animest laborum. Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque. 
				</div>
			</div>
		</section>
		<section class="categories">
			<div class="boxes">
				<a href="#" class="box" style="background-image: url('./media/images/slide_1.jpg');">
					<div class="name">KRAŠTOVAIZDIS</div>
					<div class="content">
						Lorem ipsum dolorsit aconsect adipiscing elitsed do eiusmod tempor incididunt ut labore et dolore magna
					</div>
				</a>
				<a href="#" class="box" style="background-image: url('./media/images/slide_2.jpg');">
					<div class="name">PAVELDAS</div>
					<div class="content">
						Lorem ipsum dolorsit aconsect adipiscing elitsed do eiusmod tempor incididunt ut labore et dolore magna
					</div>
				</a>
				<a href="#" class="box" style="background-image: url('./media/images/slide_3.jpg');">
					<div class="name">ATEITIS</div>
					<div class="content">
						Lorem ipsum dolorsit aconsect adipiscing elitsed do eiusmod tempor incididunt ut labore et dolore magna
					</div>
				</a>
			</div>
		</section>
	</div>

<?php include './partials/footer.php';?>